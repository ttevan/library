package com.konex.book;

import com.konex.book.entity.Book;
import com.konex.book.repository.BookRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;

@DataJpaTest
public class BookRepositoryMockTest {

    @Autowired
    private BookRepository bookRepository;

    /*
        Esta prueba unitaria retorna un libro por id
    */
    @Test
    public void findById() {
        Book book = Book.builder()
                .name("prueba")
                .price(12000)
                .description("")
                .stock(Double.parseDouble("10"))
                .coverRoute("")
                .createAt(new Date()).build();

        bookRepository.save(book);

        List<Book> founds = bookRepository.findByName(book.getName());

        Assertions.assertThat(founds.size()).isEqualTo(1);
    }
}
