package com.konex.book.controller;

import com.konex.book.entity.Book;
import com.konex.book.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RequestMapping("/books")
@RestController
public class BookRest {

    @Autowired
    private BookService bookService;

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<Book>> listBook() {
        List<Book> books = bookService.listAllBook();
        if (books.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(books);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Book> getBook(@PathVariable("id") Long id) {
        Book book = bookService.getBook(id);
        if (book == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(book);
    }

    @PostMapping
    public ResponseEntity<Book> create(@RequestBody Book book) {
        Book bookCreate = bookService.createBook(book);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookCreate);
    }

    @PutMapping
    public ResponseEntity<Book> update(@RequestBody Book book) {
        Book bookDB = bookService.updateBook(book);
        if (bookDB == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(bookDB);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<Book> delete(@PathVariable("id") Long id) {
        Book book = bookService.deleteBook(id);
        if (book == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(book);
    }

    @GetMapping(value="/{id}/{quantity}/stock")
    public ResponseEntity<Book> updateStock(@PathVariable Long id, @PathVariable Double quantity) {
        Book book = bookService.updateStock(id, quantity);
        if (book == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(book);
    }
}
