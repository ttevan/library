package com.konex.book.service;

import com.konex.book.entity.Book;

import java.util.List;

public interface BookService {

    public List<Book> listAllBook();
    public Book getBook(Long id);

    public Book createBook(Book book);
    public Book updateBook(Book book);
    public Book deleteBook(Long id);
    public Book updateStock(Long id, Double quantity);
}
