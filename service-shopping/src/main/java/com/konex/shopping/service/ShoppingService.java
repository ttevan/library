package com.konex.shopping.service;

import com.konex.shopping.entity.Shopping;

import java.util.List;

public interface ShoppingService {
    public List<Shopping> listAll();
    public Shopping get(Long id);
    public List<Shopping> getByCustomerAndStatus(Long id, String status);
    public Shopping create(Shopping shopping);
    public Shopping updateQuantity(Long id, int quantity);
    public Shopping update(Shopping shopping);
    public Shopping delete(Long id);
}
