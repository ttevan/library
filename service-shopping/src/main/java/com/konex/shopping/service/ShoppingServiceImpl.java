package com.konex.shopping.service;

import com.konex.shopping.entity.Shopping;
import com.konex.shopping.repository.ShoppingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ShoppingServiceImpl implements ShoppingService {

    @Autowired
    ShoppingRepository shoppingRepository;

    @Override
    public List<Shopping> listAll() {
        return shoppingRepository.findAll();
    }

    @Override
    public Shopping get(Long id) {
        return shoppingRepository.findById(id).orElse(null);
    }

    @Override
    public List<Shopping> getByCustomerAndStatus(Long id, String status) {
        return shoppingRepository.findByIdCustomerAndStatus(id, status);
    }

    @Override
    public Shopping create(Shopping shopping) {
        List<Shopping> shoppingDB = shoppingRepository.findByIdCustomerAndStatus(shopping.getIdCustomer(), Shopping.ENCARRITO);
        if (!shoppingDB.isEmpty()) {
            for (Shopping item : shoppingDB) {
                if (item.getIdBook() == shopping.getIdBook()) {
                    return shopping;
                }
            }
        }
        shopping.setStatus("ACTIVO");
        shopping.setCreateAt(new Date());
        return shoppingRepository.save(shopping);
    }

    @Override
    public Shopping updateQuantity(Long id, int quantity) {
        Shopping shoppingDB = get(id);
        if (shoppingDB == null) {
            return null;
        }
        shoppingDB.setQuantity(quantity);
        return update(shoppingDB);
    }

    @Override
    public Shopping update(Shopping shopping) {
        Shopping shoppingDB = get(shopping.getId());
        if (shoppingDB == null) {
            return null;
        }
        return shoppingRepository.save(shopping);
    }

    @Override
    public Shopping delete(Long id) {
        Shopping shoppingDB = get(id);
        if (shoppingDB == null) {
            return null;
        }
        shoppingDB.setStatus("removed");
        return shoppingRepository.save(shoppingDB);
    }
}
