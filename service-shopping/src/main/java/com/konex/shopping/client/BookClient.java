package com.konex.shopping.client;

import com.konex.shopping.model.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="book-service")
@RequestMapping("/books")
public interface BookClient {

    @GetMapping(value="/{id}")
    public ResponseEntity<Book> getBook(@PathVariable("id") Long id);

    @GetMapping(value="/{id}/{quantity}/stock")
    public ResponseEntity<Book> updateStock(@PathVariable Long id, @PathVariable Double quantity);
}
