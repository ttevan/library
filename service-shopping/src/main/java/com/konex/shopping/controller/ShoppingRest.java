package com.konex.shopping.controller;

import com.konex.shopping.client.BookClient;
import com.konex.shopping.entity.Shopping;
import com.konex.shopping.model.Book;
import com.konex.shopping.service.ShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/shoppings")
@RestController
public class ShoppingRest {

    @Autowired
    ShoppingService shoppingService;

    @Autowired
    BookClient bookClient;

    @GetMapping
    public ResponseEntity<List<Shopping>> list() {
        List<Shopping> shoppings = shoppingService.listAll();
        if (shoppings.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(shoppings);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Shopping> get(@PathVariable Long id) {
        Shopping shopping = shoppingService.get(id);
        if (shopping == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shopping);
    }

    @GetMapping(value="/byCustomer/{idCustomer}")
    public ResponseEntity<List<Shopping>> getByCustomer(@PathVariable Long idCustomer) {
        List<Shopping> shoppings = shoppingService.getByCustomerAndStatus(idCustomer, Shopping.ENCARRITO);
        shoppings.forEach(element -> {
            ResponseEntity<Book> book = bookClient.getBook(element.getIdBook());
            if (book.getBody() != null)
                element.setBook(book.getBody());
        });

        if (shoppings.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shoppings);
    }

    @GetMapping(value="/quantity/{idCustomer}")
    public ResponseEntity<Integer> getQuantityByCustomer(@PathVariable Long idCustomer) {
        List<Shopping> shoppings = shoppingService.getByCustomerAndStatus(idCustomer, Shopping.ENCARRITO);
        return ResponseEntity.ok(shoppings.size());
    }

    @PostMapping
    public ResponseEntity<Shopping> create(@RequestBody Shopping shopping) {
        Shopping shoppingDB = shoppingService.create(shopping);
        ResponseEntity<Book> currentQuantity = bookClient.getBook(shoppingDB.getIdBook());
        Double total = (currentQuantity.getBody().getStock()-shoppingDB.getQuantity());
        System.out.println(total);
        bookClient.updateStock(shoppingDB.getIdBook(), total);
        return ResponseEntity.status(HttpStatus.CREATED).body(shoppingDB);
    }

    @PutMapping
    public ResponseEntity<Shopping> update(@RequestBody Shopping customer) {
        Shopping shoppingDB = shoppingService.update(customer);
        if (shoppingDB == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shoppingDB);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<Shopping> delete(@PathVariable Long id) {
        Shopping shopping = shoppingService.delete(id);
        if (shopping == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(shopping);
    }
}
