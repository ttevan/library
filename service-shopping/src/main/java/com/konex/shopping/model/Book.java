package com.konex.shopping.model;

import lombok.Data;

import java.util.Date;

@Data
public class Book {
    private Long id;
    private String name;
    private int price;
    private Double stock;
    private String coverRoute;
    private String description;
    private String status;
    private Date createAt;
}
