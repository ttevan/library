package com.konex.shopping.repository;

import com.konex.shopping.entity.Shopping;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShoppingRepository extends JpaRepository<Shopping, Long> {

    public List<Shopping> findByIdCustomer(Long idCustomer);
    public List<Shopping> findByIdCustomerAndStatus(Long idCustomer, String status);
    public List<Shopping> findByIdBook(Long idBook);
}
