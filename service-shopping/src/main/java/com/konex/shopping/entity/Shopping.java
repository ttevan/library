package com.konex.shopping.entity;

import com.konex.shopping.model.Book;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="shoppings")
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class Shopping {

    @Transient
    public final static String ENCARRITO = "EN_CARRITO";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idCustomer;
    private Long idBook;
    private Double price;
    @Transient
    private Double total;
    private String status;
    private int quantity;
    @Column(name = "create_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;

    @Transient
    private Book book;

    public Double getTotal() {
        return this.quantity * this.price;
    }
}
