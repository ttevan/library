package com.konex.customer.repository;

import com.konex.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    public Customer findByUsername(String username);

    public Customer findByUsernameAndPassword(String username, String password);
}
