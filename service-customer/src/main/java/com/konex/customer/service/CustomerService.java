package com.konex.customer.service;

import com.konex.customer.entity.Customer;

import java.util.List;

public interface CustomerService {
    public List<Customer> listAll();
    public Customer get(Long id);
    public Customer login(String user, String pass);
    public Customer create(Customer customer);
    public Customer update(Customer customer);
    public Customer delete(Long id);
}
