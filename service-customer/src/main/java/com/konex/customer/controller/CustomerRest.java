package com.konex.customer.controller;

import com.konex.customer.entity.Customer;
import com.konex.customer.service.CustomerService;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/customers")
@CrossOrigin(origins = "*")
public class CustomerRest {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<Customer>> list() {
        List<Customer> customers = customerService.listAll();
        if (customers.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(customers);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Customer> get(@PathVariable Long id) {
        Customer customer = customerService.get(id);
        if (customer == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(customer);
    }

    @PostMapping
    public ResponseEntity<Customer> create(@RequestBody Customer customer) {
        Customer customerCreate = customerService.create(customer);
        return ResponseEntity.status(HttpStatus.CREATED).body(customerCreate);
    }

    @PutMapping
    public ResponseEntity<Customer> update(@RequestBody Customer customer) {
        Customer customerDB = customerService.update(customer);
        if (customerDB == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(customerDB);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<Customer> delete(@PathVariable Long id) {
        Customer customer = customerService.delete(id);
        if (customer == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(customer);
    }

    @PostMapping("/login")
    public ResponseEntity<Customer> login(@RequestBody String data) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(data);
        String user = (String) json.get("user");
        String pass = (String) json.get("pass");
        Customer customer = customerService.login(user, pass);
        if (customer == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(customer);
    }
}
